PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE searchIndex(id INTEGER PRIMARY KEY, name TEXT, type TEXT, path TEXT);
INSERT INTO searchIndex VALUES(1,'apps API methods','Method','docs.joinmastodon.org/methods/apps.html');
INSERT INTO searchIndex VALUES(2,'accounts API methods','Method','docs.joinmastodon.org/methods/accounts.html');
INSERT INTO searchIndex VALUES(3,'statuses API methods','Method','docs.joinmastodon.org/methods/statuses.html');
INSERT INTO searchIndex VALUES(4,'timelines API methods','Method','docs.joinmastodon.org/methods/timelines.html');
INSERT INTO searchIndex VALUES(5,'notifications API methods','Method','docs.joinmastodon.org/methods/notifications.html');
INSERT INTO searchIndex VALUES(6,'search API methods','Method','docs.joinmastodon.org/methods/search.html');

INSERT INTO searchIndex VALUES(7,'instance API methods','Method','docs.joinmastodon.org/methods/instance.html');
INSERT INTO searchIndex VALUES(8,'admin API methods','Method','docs.joinmastodon.org/methods/admin.html');
INSERT INTO searchIndex VALUES(9,'proofs API methods','Method','docs.joinmastodon.org/methods/proofs.html');
INSERT INTO searchIndex VALUES(10,'oembed API methods','Method','docs.joinmastodon.org/methods/oembed.html');
CREATE UNIQUE INDEX anchor ON searchIndex (name, type, path);
CREATE INDEX __zi_name0001 ON searchIndex (name COLLATE NOCASE);
COMMIT;
