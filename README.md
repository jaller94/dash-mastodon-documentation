# Dash builder for Mastodon Documentation

Builds Mastodon Documentation as a [Dash docset](https://github.com/Kapeli/Dash-User-Contributions).

To be used with the offline documentation viewers [Dash](https://kapeli.com/dash) or [Zeal](https://zealdocs.org/).

## Use with Zeal

```sh
wget https://chrpaul.de/dash/Mastodon-docset-latest.tgz
tar -xf Mastodon-docset-latest.tgz -C ~/.local/share/Zeal/Zeal/docsets/
```

I don't want to host a self-updating feed. This docset has become an official Dash User Contribution.  
https://github.com/Kapeli/Dash-User-Contributions/issues/3750

## Where to find the docset?

The docset is built on Gitlab CI and is available as artifacts.

1. Go to a pipeline with the status "passed".
2. Click on the step "build".
3. Find the section "Job artifacts" in the sidebar and click on "Browse".
4. Download "Mastodon.tgz".

## Known issues
None
