#!/bin/sh

# Fail on error
set -e

# Download Spec
if [ ! -d documentation ]; then
  git clone --recurse-submodules --shallow-submodules --depth 1 https://github.com/mastodon/documentation.git;
fi
cd documentation

# Adapt config.toml
sed -i '1s/^/canonifyURLs = false\nrelativeURLs = true\nuglyURLs = true\n/' config.toml

# Build the page
hugo.0.130.0

cd ..

sqlite3 Mastodon.docset/Contents/Resources/docSet.dsidx < index.sql

mkdir "Mastodon.docset/Contents/Resources/Documents/docs.joinmastodon.org"
mv documentation/public/* "Mastodon.docset/Contents/Resources/Documents/docs.joinmastodon.org"

tar --exclude='.DS_Store' --exclude='.gitkeep' -cvzf Mastodon.tgz Mastodon.docset
